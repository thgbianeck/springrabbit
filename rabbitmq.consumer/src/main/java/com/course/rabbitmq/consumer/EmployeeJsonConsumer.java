package com.course.rabbitmq.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.course.rabbitmq.consumer.entity.Employee;

/**
 * Criado utilizando IntelliJ IDEA.
 * Projeto: springrabbit
 * Usuário: Thiago Bianeck (Bianeck)
 * Data: 15/04/2022
 * Hora: 14:26
 */
//@Service
public class EmployeeJsonConsumer {

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeJsonConsumer.class);

    @RabbitListener(queues = "course.employee")
    public void listen(String message) throws JsonProcessingException {
        var employee = objectMapper.readValue(message, Employee.class);

        LOG.info("Employee is {}", employee);
    }
}
