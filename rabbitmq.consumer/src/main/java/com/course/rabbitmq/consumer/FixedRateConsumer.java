package com.course.rabbitmq.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Criado utilizando IntelliJ IDEA.
 * Projeto: springrabbit
 * Usuário: Thiago Bianeck (Bianeck)
 * Data: 10/04/2022
 * Hora: 14:09
 */
//@Service
public class FixedRateConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(FixedRateConsumer.class);

    @RabbitListener(queues = "course.fixedrate", concurrency = "3-7")
    public void listen(String message) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextLong(1000, 2000));
        LOG.info("{}: Consuming {}",Thread.currentThread().getName(), message);
    }
}
