package com.course.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * Criado utilizando IntelliJ IDEA.
 * Projeto: springrabbit
 * Usuário: Thiago Bianeck (Bianeck)
 * Data: 06/04/2022
 * Hora: 21:52
 */
//@Service
public class HelloRabbitConsumer {

    @RabbitListener(queues = "course.hello")
    public void listen(String message) {
        System.out.println("Consuming " + message);
    }
}
