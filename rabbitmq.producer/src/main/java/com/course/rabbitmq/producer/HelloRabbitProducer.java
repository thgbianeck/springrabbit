package com.course.rabbitmq.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Criado utilizando IntelliJ IDEA.
 * Projeto: springrabbit
 * Usuário: Thiago Bianeck (Bianeck)
 * Data: 06/04/2022
 * Hora: 21:42
 */
@Service
public class HelloRabbitProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendHello(String name) {
        rabbitTemplate.convertAndSend("course.hello", "Hello " + name);
    }

}
