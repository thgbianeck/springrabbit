package com.course.rabbitmq.producer;

import com.course.rabbitmq.producer.entity.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Criado utilizando IntelliJ IDEA.
 * Projeto: springrabbit
 * Usuário: Thiago Bianeck (Bianeck)
 * Data: 15/04/2022
 * Hora: 14:12
 */
@Service
public class EmployeeJsonProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public void sendMessage(Employee data) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(data);

        rabbitTemplate.convertAndSend("course.employee", json);
    }
}
